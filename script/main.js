// Main Loader Functions

function load_toc() {
  $("#main").load("/template/toc.html");
}

function load_about() {
  $("#main").load("/template/about.html");
  window.clicked = 1;
}

function load_coding() {
  $("#main").load("/template/coding.html");
  window.clicked = 1;
}

function load_music() {
  $("#main").load("/template/music.html");
  window.clicked = 1;
}

function load_travel() {
  $("#main").load("/template/travel.html");
  window.clicked = 1;
}
