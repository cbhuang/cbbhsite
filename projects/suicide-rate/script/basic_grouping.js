//
//  basic grouping (histograms)
//

function initBasicGrouping() {
  $("#main").load("template/basic_grouping.html", function (){
    initBasicGroupingByGdpPcGroup5();
  });
}

//
//  1. GDP
//
function initBasicGroupingByGdpPcGroup5() {

  // // the user-selected value:
  let data = window.dataByGdpPcGroup5;
  data.sort( (a,b) => d3.ascending(a.gdpPcGroup5, b.gdpPcGroup5) );

  // data
  let data_plot = [{
    type: 'bar',
    x: data.map( (d) => {return d["gdpPcGroup5"]}),
    y: data.map( (d) => {return d["srate"]}),
    text: data.map( (d) => {return `Group ${d["gdpPcGroup5"]}: Mean GDP per Capita=${d["gdpPc"].toFixed(1)}`} ),
    marker: {
      color: "#350c96",
      opacity: 0.8
    }
  }];

  // layout
  let layout = {
    title: "Suicide Rate (by GDP)",
    showlegend: false,
    width: 800,
    height: 600,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 24,
    },
    xaxis: {
      title: "GDP Quintile",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-basic-grouping", data_plot, layout);
}

//
// 2. by Year
//
function initBasicGroupingByYear() {

  // // the user-selected value:
  let data = window.dataByYear;

  // data
  let data_plot = [{
    type: 'scatter',
    x: data.map( (d) => {return d["year"]}),
    y: data.map( (d) => {return d["srate"]}),
    marker: {
      color: "#350c96",
    },
  }];

  // layout
  let layout = {
    title: "Suicide Rate (by Year)",
    showlegend: false,
    width: 800,
    height: 600,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 24,
    },
    xaxis: {
      title: "Year",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-basic-grouping", data_plot, layout);
}

//
// 3. by Age
//
function initBasicGroupingByAge() {

  // // the user-selected value:
  let data = window.dataByAge;

  // data
  let data_plot = [{
    type: 'bar',
    x: data.map( (d) => {return d["age"]}),
    y: data.map( (d) => {return d["srate"]}),
    marker: {
      color: "#350c96",
      opacity: 0.8
    }
  }];

  // layout
  let layout = {
    title: "Suicide Rate (by Age)",
    showlegend: false,
    width: 800,
    height: 600,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 24,
    },
    xaxis: {
      //title: "Age",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-basic-grouping", data_plot, layout);
}

//
// 4. by Sex
//
function initBasicGroupingBySex() {

  // // the user-selected value:
  let data = window.dataBySex;

  // data
  let data_plot = [{
    type: 'pie',
    values: [data.filter( (d) => {if (d.sex == 'F') {return true;} else {return false;}} )[0].srate.toFixed(2),
             data.filter( (d) => {if (d.sex == 'M') {return true;} else {return false;}} )[0].srate.toFixed(2)],
    labels: ['Female', 'Male']
  }];

  // layout
  let layout = {
    title: "Suicide Rate (by Sex)",
    // showlegend: false,
    width: 800,
    height: 600,
    font: {
      size: 24,
    },
    annotations: [
      {
        xref: 'paper',
        yref: 'paper',
        x: 0.55,
        y: 1.09,
        xanchor: 'center',
        yanchor: 'center',
        text: `per 100k Person per Year`,
        font:{
          size: 18,
        },
        showarrow: false
      }
    ]
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-basic-grouping", data_plot, layout);
}

//
//  5. Age-Sex
//
function initBasicGroupingByAgeSex() {

  // double grouping

  let data = window.dataByAgeSex;

  // stacked components
  let y1 = data.filter( (d) => {return d.sex == "M"})
    .sort( (a, b) => d3.ascending(a.age, b.age) )
    .map( (d) => {return d["srate"]});
  let trace1 = {
    x: window.ls_age,
    y: y1,
    name: 'Male',
    type: 'bar',
  };

  let y2 = data.filter( (d) => {return d.sex == "F"})
    .sort( (a, b) => d3.ascending(a.age, b.age) )
    .map( (d) => {return d["srate"]});
  let trace2 = {
    x: window.ls_age,
    y: y2,
    name: 'Female',
    type: 'bar',
    // marker: {
    //   color: "#350c96",
    //   opacity: 0.8
    // },
  };

  let data_plot = [trace1, trace2];

  // layout
  let layout = {
    title: "Suicide Rate (by Age-Sex)",
    barmode: 'stack',
    //showlegend: false,
    width: 800,
    height: 600,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 24,
    },
    xaxis: {
      //title: "Age",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
  };

  // Refresh figure (.plot() will mess up legend)
  Plotly.newPlot("fig-basic-grouping", data_plot, layout);
}
