//
// initialization
//

// master initialization functions
function initialize() {

  // load and preprocess the suicide dataset
  d3.csv("data/load.csv", function(data){
    // format numbers
    return {
      country: data.country,
      year: +data.year,
      sex: data.sex,
      age: data.age,
      sn: +data.suicides_no,
      population: +data.population,
      srate: +data.srate,
      country_year: data.country_year,
      gdp_million: +data.gdp_million,
      gdpPc: +data.gdpPc,
      generation: data.generation
    };
  })
  .then(function(data) {
    // preprocession

    // 2. generate dropdown lists
    getDistinctList(data, "year", reverse=true);
    getDistinctList(data, "country");
    getDistinctList(data, "age");
    getDistinctList(data, "generation");
    getDistinctList(data, "sex");

    // 3. compute quantiles
    // gdp quintile for each year
    // n.b. group by sex and age to ensure consistency (99 nations each group).
    //      This is so inefficient. But separating GDP per country and merging
    //      it back to the original table is also a headache.

    // gdpPc by year
    data = ntileGroupBy3(data, "gdpPc", ["year", "sex", "age"], "gdpPcGroup5ByYear", desc=false, n=5);
    // gdpPc (overall)
    data = addGdpPcGroup(data, n=5);

    // 4. aggregated datasets
    // overall
    groupByNone(data);
    // single variable
    groupBy1(data, "year");
    groupBy1(data, "country");  // done before
    groupBy1(data, "age");
    window.dataByAge.sort( (a, b) => d3.ascending(a.age, b.age) );
    groupBy1(data, "generation");
    groupBy1(data, "sex");
    groupBy1(data, "gdpPcGroup5");

    // double vars
    groupBy2(data, "country", "year");
    groupBy2(data, "country", "age");
    groupBy2(data, "country", "generation");
    groupBy2(data, "country", "sex");
    groupBy2(data, "age", "sex")

    // triple vars
    groupBy3(data, "country", "age", "sex");

    // average population
    window.avgPopulation = computeAvgPopulation();

    // 5. sort and save to global environment
    // (n.b. localStorage cannot store json object!)
    window.data = alasql(`
      select *
      from ?
      order by country, year, age, sex
      `, [data]);

    // 6. load alcoholic dataset
    initAlcoholData();

    // 99. load intro page
    $("#main").load("template/intro.html");
    console.log("Raw data reloaded!");
  });
}


function addGdpPcGroup(data, n=5) {
  // add overall gdp grouping
  //
  // data: raw data
  // n: quantile count (default=5)

  // step 1: get average
  let tmp1 = alasql(`
    select country,
           avg(gdpPc) as gdpPc
    from ?
    group by country
    `, [data]);  // no need to sort

  // step 2. compute ntile() which is not implemented in alaSQL
  let tmp2 = ntileGroupByNone(tmp1, "gdpPc", "gdpPcGroup5", desc=false, n=n);

  // step 3. merge ntile data back
  return alasql(`
    select A.*,
           B.gdpPc as gdpPcAvg,
           B.gdpPcGroup5
    from ? as A
    left join ? as B
        on A.country = B.country
    order by country
    `, [data, tmp2]);
}

// compute agerage population
function computeAvgPopulation() {

  let data = window.dataByCountryYear;
  let tmp_data = d3.nest()
    .key((d) => { return d.country; })
    .rollup(function(v) { return {
        population: d3.mean(v, (d) => { return d.population; } ),
      };})
    .object(data);

  // unpack nest
  let ans = [];
  let tmp_obj;
  for (const key of Object.keys(tmp_data)) {  // ES6
    tmp_obj = {};
    // from tmp_data
    tmp_obj["country"] = key;
    tmp_obj["population"] = tmp_data[key]["population"];
    ans.push(tmp_obj);
  }
  ans.sort( (a, b) => d3.ascending(a.country, b.country));
  return ans;
}

// load alcoholic consumption data, and merge into the main dataset
function initAlcoholData() {

  d3.csv("data/load_alcohol.csv", function(data){
    // format data
    return {
      country: data.country,
      year: +data.year,
      liter: +data.liters_per_capita
    };
  })
  .then(function(alcoholData) {

    // kill data
    window.dataAlcohol = alasql(`
      select A.country,
             A.year,
             B.liter
      from ? as A
      left join ? as B
          on A.country = B.country
          and A.year = B.year
      order by country, year
      `, [window.dataByCountryYear, alcoholData]);

    console.log("Alcohol consumption data loaded!");
  });
}
