/**
 * Load data into global environment
 */

// inspect
function tabulate() {

  // create element to be appended
  $("#main").html(
    `<h2>Interactive Data Inspection</h2>
     <div id="dataTable">
       <h2>(!! Data loader failed !!)</h2>
     </div>`
   );

  // tabulator.js
  window.mainTable = new Tabulator("#dataTable", {
    height: 520,  // strongly suggested for efficiency
    data: window.data,
    layout: "fitDataFill",  // fill spaces if any
    columns:[   // Must manually define columns to be displayed
      {title:"Country", field:"country", headerFilter:true},  // title and field are required
      {title:"Year", field:"year", headerFilter:true},
      {title:"Country-Year", field:"country_year"},
      {title:"Sex", field:"sex", headerFilter:true},
      {title:"Age", field:"age", headerFilter:true},
      //{title:"Generation", field:"generation", headerFilter:true},
      {title:"#Suicides", field:"sn"},
      {title:"Suicide Rate", field:"srate"},
      {title:"Population", field:"population"},
      {title:"GDP (Million USD)", field:"gdp_million"},
      {title:"GDP per Capita", field:"gdpPc"},
      {title:"GDPpC Quintile (by year)", field:"gdpPcGroup5ByYear", headerFilter:true},
      {title:"GDP per Capita (Time Avg)", field:"gdpPcAvg"},
      {title:"GDPpC Quintile (Time Avg)", field:"gdpPcGroup5", headerFilter:true},
    ],
    tooltips: true,
    tooltipsHeader:true,
    pagination: "local",
    paginationSize: 14,
  });
}
