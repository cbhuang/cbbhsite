//
//  Detailed View within Countries
//

function initCountryView() {

  $("#main").load("template/country_view.html", function (){

    // make choropleth and dropdown list
    initWorldmapCountries();
    // initial viewTab for fast-switching
    window.countryViewTab = "byNone";
    // set initial country
    $("#countryDropdown").val("Netherlands").trigger('change');
  });
}

// init world map as country selector
function initWorldmapCountries() {
  // create dropdowns
  createDropdown("country");  // the onchange event is already binded
  updateWorldmapCountries();
}

// update world map colorization
function updateWorldmapCountries() {

  // decide which color to show
  let color_option = $("#colorizeDropdown").val();
  let z_raw;
  let annotation_caption;
  if (color_option == "gdpPc") {
    z_raw = unpackArray(window.dataByCountry, 'gdpPc');
    annotation_caption = "(Color: Average GDP per Capita '90-'13)";
  } else if (color_option == "srate") {
    z_raw = unpackArray(window.dataByCountry, 'srate');
    annotation_caption = "(Color: Average Suicide Rate '90-'13)";
  }

  let i = 0;
  let z = [];
  while (i < z_raw.length) {
    z.push(z_raw[i].toFixed(1));
    ++i;
  }

  // data to plot
  let data_plot = [{
      type: 'choropleth',
      locationmode: 'country names',
      locations: window.ls_country,
      z: z,
      // text: hovertext,
      colorscale: 'Picnic',
  }];

  // layout
  let layout = {
      geo: {
          projection: {
              type: 'equirectangular'
          }
      },
      height: 300,
      width: 450,
      margin: {
        t: 10,
        b: 10,
        l: 10,
        r: 10
      },
      font: {
        size: 16,
      },
      annotations: [
        {
          xref: 'paper',
          yref: 'paper',
          x: 0.55,
          y: 1.0,
          xanchor: 'center',
          yanchor: 'center',
          text: annotation_caption,
          font:{
            size: 16,
          },
          showarrow: false
        }
      ]
  };

  // Construct figure (.plot() will mess up legend)
  Plotly.newPlot("countryMap", data_plot, layout, {showLink: false});

  // Bind Events to the figure
  // See https://plot.ly/javascript/click-events/#click-event-data for the event data structure
  document.getElementById("countryMap").on('plotly_click', function(data) {
    let i = 0;
    let idx;
    let country;
    // loop through all selected data points, but here data.points.length == 1 (always)
    while (i < data.points.length) {
      idx = data.points[i].pointNumber;
      country = data.points[i].data.locations[idx];  // get selected country
      ++i;
    }
    // renew countryDropdown and trigger onchange event
    $("#countryDropdown").val(country).trigger('change');
  });
}

// show details
function countryDropdownOnchange() {

  if (window.countryViewTab == "byNone") {
    initCountryByNone();
  } else if (window.countryViewTab == "byAgeSex") {
    initCountryByAgeSex();
  } else if (window.countryViewTab == "byYear") {
    initCountryByYear();
  } else if (window.countryViewTab == "byAge") {
    initCountryByAge();
  } else if (window.countryViewTab == "bySex") {
    initCountryBySex();
  } else {
    alert(`window.countryViewTab = ${window.countryViewTab} is not implemented!`);
  }
}

//
// 1. overall statistics
//
function initCountryByNone() {

  let country = $("#countryDropdown").val();
  window.countryViewTab = "byNone";

  // show overall data
  let data = window.dataByCountry.filter( (d) => { if (d.country == country) {return true;} else {return false;}} )[0];
  
  $("#fig-countryDetails").html(
    `<div style='text-align: center'>
       <br>
       <h2>${country}</h2>
       <h4>Suicide Rate = ${data.srate.toFixed(2)} (per 100k person per year)</h4>
     </div>`);
}

//
// 2. Aggregate by age-sex
//
function initCountryByAgeSex() {

  let country = $("#countryDropdown").val();
  window.countryViewTab = "byAgeSex";

  // show overall data
  let data = window.dataByCountryAgeSex.filter( (d) => { if (d.country == country) {return true;} else {return false;}} );

  // data to plot
  // stacked components
  let y1 = data.filter( (d) => {return d.sex == "M"})
    .sort( (a, b) => d3.ascending(a.age, b.age) )
    .map( (d) => {return d["srate"]});
  let trace1 = {
    x: window.ls_age,
    y: y1,
    name: 'Male',
    type: 'bar',
  };

  let y2 = data.filter( (d) => {return d.sex == "F"})
    .sort( (a, b) => d3.ascending(a.age, b.age) )
    .map( (d) => {return d["srate"]});
  let trace2 = {
    x: window.ls_age,
    y: y2,
    name: 'Female',
    type: 'bar',
  };

  let data_plot = [trace1, trace2];

  // layout
  let layout = {
    title: `Suicide Rate (by Age-Sex)`,
    barmode: 'stack',
    showlegend: true,
    width: 600,
    height: 480,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 22,
    },
    xaxis: {
      //title: "Year",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
    annotations: [
      {
        xref: 'paper',
        yref: 'paper',
        x: 0.55,
        y: 1.12,
        xanchor: 'center',
        yanchor: 'center',
        text: `${country}`,
        font:{
          size: 20,
        },
        showarrow: false
      }
    ]
  };

  // Refresh figure (.plot() will mess up legend)
  $("#fig-countryDetails").empty();
  Plotly.newPlot("fig-countryDetails", data_plot, layout);
}

//
// 3. Aggregate by year
//
function initCountryByYear() {

  let country = $("#countryDropdown").val();
  window.countryViewTab = "byYear";

  // show overall data
  let data = window.dataByCountryYear.filter( (d) => { if (d.country == country) {return true;} else {return false;}} );
  data.sort( (a, b) => d3.ascending(a.year, b.year) );

  // plot
  let data_plot = [{
    type: 'scatter',
    x: data.map( (d) => {return d["year"]}),
    y: data.map( (d) => {return d["srate"]}),
    marker: {
      color: "#350c96",
    },
  }];

  // layout
  let layout = {
    title: `Suicide Rate (by Year)`,
    showlegend: false,
    width: 600,
    height: 480,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 22,
    },
    xaxis: {
      title: "Year",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
    annotations: [
      {
        xref: 'paper',
        yref: 'paper',
        x: 0.5,
        y: 1.12,
        xanchor: 'center',
        yanchor: 'center',
        text: `${country}`,
        font:{
          size: 20,
        },
        showarrow: false
      }
    ]
  };

  // Refresh figure (.plot() will mess up legend)
  $("#fig-countryDetails").empty();
  Plotly.newPlot("fig-countryDetails", data_plot, layout);
}

//
// 4. Aggregate by sex
//
function initCountryBySex() {

  let country = $("#countryDropdown").val();
  window.countryViewTab = "bySex";

  // show overall data
  let data = window.dataByCountrySex.filter( (d) => { if (d.country == country) {return true;} else {return false;}} );
  data.sort( (a, b) => d3.ascending(a.sex, b.sex) );

  // plot
  let data_plot = [{
    type: 'pie',
    values: [data.filter( (d) => {if (d.sex == 'F') {return true;} else {return false;}} )[0].srate.toFixed(2),
             data.filter( (d) => {if (d.sex == 'M') {return true;} else {return false;}} )[0].srate.toFixed(2)],
    labels: ['Female', 'Male']
  }];

  // layout
  let layout = {
    title: `Suicide Rate (by Sex)`,
    // showlegend: false,
    width: 600,
    height: 480,
    font: {
      size: 22,
    },
    annotations: [
      {
        xref: 'paper',
        yref: 'paper',
        x: 0.5,
        y: -0.1,
        xanchor: 'center',
        yanchor: 'center',
        text: `per 100k Person per Year`,
        font:{
          size: 18,
        },
        showarrow: false
      },
      {
        xref: 'paper',
        yref: 'paper',
        x: 0.55,
        y: 1.12,
        xanchor: 'center',
        yanchor: 'center',
        text: `${country}`,
        font:{
          size: 20,
        },
        showarrow: false
      }
    ]
  };

  // Refresh figure (.plot() will mess up legend)
  $("#fig-countryDetails").empty();
  Plotly.newPlot("fig-countryDetails", data_plot, layout);
}

//
// 5. Aggregate by age
//
function initCountryByAge() {

  let country = $("#countryDropdown").val();
  window.countryViewTab = "byAge";
  // show overall data
  let data = window.dataByCountryAge.filter( (d) => { if (d.country == country) {return true;} else {return false;}} );
  data.sort( (a, b) => d3.ascending(a.age, b.age) );

  // plot
  let data_plot = [{
    type: 'bar',
    x: data.map( (d) => {return d["age"]}),
    y: data.map( (d) => {return d["srate"]}),
    marker: {
      color: "#350c96",
    },
  }];

  // layout
  let layout = {
    title: `Suicide Rate (by Age)`,
    showlegend: false,
    width: 600,
    height: 480,
    // margin: {
    //   t: 100,
    // },
    font: {
      size: 22,
    },
    xaxis: {
      //title: "Year",
      tickfont: {
        size: 18,
      },
      titlefont: {
        size: 18,
      },
    },
    yaxis: {
      title: 'Suicide Rate per 100k Person per Year',
      titlefont: {
        size: 18,
      },
      tickfont: {
        size: 18,
      }
    },
    annotations: [
      {
        xref: 'paper',
        yref: 'paper',
        x: 0.5,
        y: 1.12,
        xanchor: 'center',
        yanchor: 'center',
        text: `${country}`,
        font:{
          size: 20,
        },
        showarrow: false
      }
    ]
  };

  // Refresh figure (.plot() will mess up legend)
  $("#fig-countryDetails").empty();
  Plotly.newPlot("fig-countryDetails", data_plot, layout);
}
